package main

import (
	"fmt"
	"io"
	"os"
)

const (
	corner  = "└"
	line    = "───"
	tee     = "├"
	pipline = "│"
)

func main() {
	out := os.Stdout
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}
	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	err := dirTree(out, path, printFiles)
	if err != nil {
		panic(err.Error())
	}
}

// Реализации ф-и предпологаем что мы читаем файл проверяем является ли он директорией. Если файл является директорией мы его отправляем в рекурсивную проверку. Хотя зачем нам проверять 300 раз то что и так уже известо. Нам передали директорию. Можно проверять единоравзова при входе. Если же там директория то отправлять в ф-ю которая принимать должна только директории.
// Вопрос когда мы получаем информацию о файлах находящихся в директории в каком формате они приходят?
// Отсортированы ли они, если они не отсортированы то нам необходимо их отсортировать. По имени.

func dirTree(out io.Writer, path string, f bool) error {

	//Обрабатываем путь который к нам пришёл из path
	// Для начала открое папку с помощью os.Open
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	stat, err := file.Stat()
	if err != nil {
		return err
	}
	err = dirInfo("", ".", stat, out, f)
	if err != nil{
		return err
	}

	return nil
}

func dirInfo(prefix, path string, file os.FileInfo, out io.Writer, F bool) error {
	var dir = path + "/" + file.Name()
	var files []os.FileInfo
	f, err := os.Open(dir)
	if err != nil {
		return err
	}
	ff, err := f.Readdir(-1)
	if err != nil {
		return err
	}

	for i := range ff{
		if ff[i].IsDir(){
			files = append(files,ff[i])
		}else if F {
			files = append(files,ff[i])
		}
	}
	var nowPrefix string
	var rePrefix string
	for i := range files {
		if i == len(files)-1 {
			nowPrefix = corner + line
			rePrefix = prefix + "\t"
		}else{
			nowPrefix = tee + line
			rePrefix = prefix + pipline + "\t"
		}
		if files[i].IsDir() {
			_, _ = fmt.Fprintf(out, "%v%v%v\n", prefix, nowPrefix, files[i].Name())
			err := dirInfo(rePrefix, dir, files[i], out, F)
			if err != nil{
				return err
			}
		}else if F{
			var size string
			s := files[i].Size()
			if s == 0 {
				size = "empty"
			}else{
				size = fmt.Sprintf("%vb", s)
			}
			_, _ = fmt.Fprintf(out, "%v%v%v (%v)\n", prefix, nowPrefix, files[i].Name(), size)
		}
	}
	return nil
}